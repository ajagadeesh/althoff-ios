/* HistoryTableViewController.m
 *
 * Copyright (C) 2009  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or   
 *  (at your option) any later version.                                 
 *                                                                      
 *  This program is distributed in the hope that it will be useful,     
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of      
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 *  GNU General Public License for more details.                
 *                                                                      
 *  You should have received a copy of the GNU General Public License   
 *  along with this program; if not, write to the Free Software         
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */     

#import "RSSTableViewController.h"
#import "RSSTableViewCell.h"
#import "RSSTableViewCellImgCell.h"
#import "LinphoneManager.h"
#import "PhoneMainView.h"
#import "UACellBackgroundView.h"
#import "UILinphone.h"
#import "Utils.h"
#import "Reachability.h"
#import "ServiceDetailsViewController.h"
#import "Toast+UIView.h"

@implementation ModelRSS
@synthesize content;
-(id)initWithContent:(NSString *)theContent
       andURLAddress:(NSString *)theURLAddress
         andURLImage:(NSString *)theURLImage
{
    self = [self init];
    if (self) {
        self.content = theContent;
        self.URLAddress = theURLAddress;
        self.URLImage = theURLImage;
    }
    
    return self;
}
@end

@implementation ModelRSSService
@synthesize title;
@synthesize URLAddress;
@synthesize number;
-(id)initWithContent:(NSString *)theContent
            AndTitle:(NSString *)theTitle
         andURLImage:(NSString *)theURLImage
       andURLAddress:(NSString *)theURLAddress
           andNumber:(NSString *)theNumber
{
    self = [self init];
    if (self) {
        self.content = theContent;
        self.title = theTitle;
        self.URLImage = theURLImage;
        self.URLAddress = theURLAddress;
        self.number = theNumber;
    }
    
    return self;
}
@end

@implementation RSSTableViewController

@synthesize missedFilter;
@synthesize isService;
@synthesize rssAddress = _rssAddress;

#pragma mark - Lifecycle Functions

- (void)initHistoryTableViewController {
    rssEntries = [[NSMutableArray alloc] init];
    rssEntriesLoaded = [[NSMutableArray alloc] init];
    missedFilter = false;
    isService = NO;
    
    alreadyWarned = NO;
    NSLog(@"First load");

}

- (id)init {
    self = [super init];
    if (self) {
        NSLog(@"init");
        alreadyLoaded = NO;
		[self initHistoryTableViewController];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        NSLog(@"initWithCoder");
        alreadyLoaded = NO;
		[self initHistoryTableViewController];
	}
    return self;
}	

- (void)dealloc {
    [rssEntries release];
    [rssEntriesLoaded release];
    [super dealloc];
}


#pragma mark - ViewController Functions 

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillApper");
    if (!alreadyLoaded) {
        [super viewWillAppear:animated];
        /*
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
        
         
         */

        NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
        rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        self.rssAddress = [[LinphoneManager instance] lpConfigStringForKey:@"condominio" forSection:@"rss"];
        self.rssAddress = [self.rssAddress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        self.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.rssAddress];

        self.isService = NO;
        [self loadRSS];
        alreadyLoaded = YES;
    } 
}

- (BOOL)firstAuth {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *rssAuthURL = [[LinphoneManager instance] lpConfigStringForKey:@"auth_url" forSection:@"rss"];
    rssAuthURL = [rssAuthURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    rssAuthURL = [NSString stringWithFormat:@"%@%@", rootURL, rssAuthURL];

    NSLog(@"RSS Auth: %@", rssAuthURL);

    NSURL *url = [NSURL URLWithString:rssAuthURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    NSString *rssAuthUser = [defaults objectForKey:@"username"];
    NSString *rssAuthPass = [defaults objectForKey:@"password"];

    if (!rssAuthPass || !rssAuthPass) {
        NSLog(@"Username or Password empty");
        return NO;
    }

    NSString *rssAuthString = [rssAuthUser stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *rssPassStr = [rssAuthPass stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *strData = [NSString stringWithFormat:@"exten=%@&password=%@", rssAuthString, rssPassStr];
    NSData *postData = [strData dataUsingEncoding:[NSString defaultCStringEncoding]];

    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    request.timeoutInterval = 5;

    [[NSURLConnection alloc] initWithRequest:request delegate:self];

    return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSLog(@"Response %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    [self activityIndicator];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*   )cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Error %@", error.description);
    [self activityIndicator];
}

-(void)loadRSS {
    NSLog(@"Call loadRSS");
    [self.view makeToastActivity];
    [self firstAuth];
}

-(void)activityIndicator {
    self.tableView.userInteractionEnabled = NO;
    NSLog(@"Parsing URL: %@", self.rssAddress);
    RSSParser *parser = [[RSSParser alloc] initWithUrl:self.rssAddress synchronous:NO];
    parser.delegate = self;
    [parser parse];
}

/*
- (void) reachabilityChanged: (NSNotification* )note
{
    NSLog(@"Reachability Changed");
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    switch (netStatus) {
        case NotReachable: {
            if (alreadyWarned == NO) {
                NSLog(@"No internet connection");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Falha na conexão"
                                                                message:@"Erro ao conectar com a internet"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [self.view hideToastActivity];
                alreadyWarned = YES;
            }
            
            self.tableView.userInteractionEnabled = YES;
        } break;
        case ReachableViaWWAN:
        case ReachableViaWiFi: {
            NSLog(@"WILL PARSE FEED");

            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
            });
            

        } break;
        default:
            break;
    }
    
    [hostReach stopNotifier];
}
 */

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


#pragma mark - Event Functions

-(void)rssParser:(RSSParser *)parser parsedFeed:(RSSFeed *)feed {
    NSLog(@"rssParser feed %@", [feed description]);

    
    NSString *author = [feed author];
    NSLog(@"Author: %@", author);
    
    [rssEntries removeAllObjects];
    
    NSMutableArray *articles = [feed articles];
    for (RSSEntry *entry in articles) {
        NSLog(@"Entry %@", entry.author);
        NSLog(@"Title %@", entry.description);
        
        ModelRSS *rssEntry = nil;
        
        if (self.isService) {
            rssEntry = [[ModelRSSService alloc] initWithContent:entry.summary
                                                       AndTitle:entry.title
                                                    andURLImage:entry.image
                                                  andURLAddress:entry.url
                                                      andNumber:entry.number];
        } else {
            rssEntry = [[ModelRSS alloc] initWithContent:entry.summary
                                           andURLAddress:entry.url
                                             andURLImage:entry.image];
        }
        
        [rssEntries addObject:rssEntry];
    }
    
    [[self tableView] reloadData];
    [self performSelector:@selector(delayedReloadData)];
}

-(void)delayedReloadData{
    [self.view hideToastActivity];
    self.tableView.userInteractionEnabled = YES;
}

-(void)rssParser:(RSSParser *)parser errorOccurred:(NSError *)error {
    NSLog(@"rssParser feed error: %@", error);
   [self.view hideToastActivity];
    self.tableView.userInteractionEnabled = YES;
    if (alreadyWarned == NO) {
        /*
        NSLog(@"No internet connection");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Falha na conexão"
                                                        message:@"Erro ao conectar com a internet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self.view hideToastActivity];
         */
        alreadyWarned = YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [rssEntries count];
}

#define FONT_SIZE 14.0f
#define CELL_CONTENT_MARGIN 5.0f
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    ModelRSS *entry = [rssEntries objectAtIndex:[indexPath row]];
//    NSString *text = entry.content;
   // CGSize constraint = CGSizeMake([[UIScreen mainScreen] bounds].size.width - (CELL_CONTENT_MARGIN * 2), 20000.0f);
   // CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
  //  CGFloat height = MAX(size.height, 60.0f);
//    return height + (CELL_CONTENT_MARGIN * 2);
    CGFloat height = 80.0f;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isService) {
        static NSString *kCellId = @"RSSTableViewCellImg";
        RSSTableViewCellImgCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
        if (cell == nil) {
            cell = [[[RSSTableViewCellImgCell alloc] initWithIdentifier:kCellId] autorelease];
            // Background View
            UACellBackgroundView *selectedBackgroundView = [[[UACellBackgroundView alloc] initWithFrame:CGRectZero] autorelease];
            cell.selectedBackgroundView = selectedBackgroundView;
            [selectedBackgroundView setBackgroundColor:LINPHONE_TABLE_CELL_BACKGROUND_COLOR];
        }
        
        ModelRSSService *entry = [rssEntries objectAtIndex:[indexPath row]];
        [cell setupTitle: entry.title];
        [cell setupImgURL:entry.URLImage];

        NSLog(@"Load cell for index %d", indexPath.row);
        return cell;
    } else {
        static NSString *kCellId = @"RSSTableViewCell";
        RSSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
        if (cell == nil) {
            cell = [[[RSSTableViewCell alloc] initWithIdentifier:kCellId] autorelease];
            // Background View
            UACellBackgroundView *selectedBackgroundView = [[[UACellBackgroundView alloc] initWithFrame:CGRectZero] autorelease];
            cell.selectedBackgroundView = selectedBackgroundView;
            [selectedBackgroundView setBackgroundColor:LINPHONE_TABLE_CELL_BACKGROUND_COLOR];
        }

        ModelRSS *entry = [rssEntries objectAtIndex:[indexPath row]];
        [cell setupTitle: entry.content];
        [cell setupURLS: entry.URLAddress];
        [cell setupImageURL:entry.URLImage];
        NSLog(@"Load cell for index %d", indexPath.row);
        return cell;
    }
    
    return NULL;
}


#pragma mark - UITableViewDelegate Functions



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"Select at index: %d", indexPath.item);
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (self.isService) {
        ServiceDetailsViewController *controller = DYNAMIC_CAST([[PhoneMainView instance] changeCurrentView:[ServiceDetailsViewController compositeViewDescription]  push:TRUE], ServiceDetailsViewController);

        ModelRSSService *entry = [rssEntries objectAtIndex:[indexPath row]];
        [controller setupWithImageURL:entry.URLImage
                              contact:entry.title
                          contactLink:entry.URLAddress
                        contactNumber:entry.number
                   contactDescription:entry.content];
    }else{
        
        ModelRSS *entry = [rssEntries objectAtIndex:[indexPath row]];
        _urlAddress = entry.URLAddress;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                      @"([0-9]+):([a-zA-Z]+)" options:0 error:nil];
        
        NSArray *matches = [regex matchesInString:self.urlAddress
                                          options:0
                                            range:NSMakeRange(0, [self.urlAddress length])];
        
        NSTextCheckingResult *match = matches[0];
        NSRange firstHalfRange = [match rangeAtIndex:1];
        NSRange secondHalfRange = [match rangeAtIndex:2];
        
        
        NSString *newURL = [regex stringByReplacingMatchesInString:self.urlAddress options:0 range:NSMakeRange(0, self.urlAddress.length) withTemplate:[NSString stringWithFormat:@"%@&from=%@", [self.urlAddress substringWithRange:firstHalfRange], [self.urlAddress substringWithRange:secondHalfRange]]];
        
        
        WebViewController *controller = DYNAMIC_CAST([[PhoneMainView instance] changeCurrentView:[WebViewController compositeViewDescription]  push:TRUE], WebViewController);
        
        [controller setupURLS: newURL];
    }
    

}

@end

