//
//  ServiceDetailsViewController.h
//  softphone
//
//  Created by Eduardo Nunes Pereira on 16/7/13.
//
//

#import <UIKit/UIKit.h>
#import "UICompositeViewController.h"

@interface ServiceDetailsViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIImageView *image;
@property (retain, nonatomic) IBOutlet UILabel *contactName;
@property (retain, nonatomic) IBOutlet UILabel *contactLink;
@property (retain, nonatomic) IBOutlet UILabel *contactNumber;
@property (retain, nonatomic) IBOutlet UITextView *contactDescription;


+ (UICompositeViewDescription *)compositeViewDescription;
-(void) setupWithImageURL:(NSString*) theURL
                  contact:(NSString*) theContact
              contactLink:(NSString*) theContactLink
            contactNumber:(NSString*) theContactNumber
       contactDescription:(NSString*) theContactDescription;
@end
