/* HistoryViewController.h
 *
 * Copyright (C) 2012  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or   
 *  (at your option) any later version.                                 
 *                                                                      
 *  This program is distributed in the hope that it will be useful,     
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of      
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 *  GNU General Public License for more details.                
 *                                                                      
 *  You should have received a copy of the GNU General Public License   
 *  along with this program; if not, write to the Free Software         
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */              

#import <UIKit/UIKit.h>

#import "UICompositeViewController.h"
#import "WebViewController.h"
#import "RSSTableViewController.h"
#import "UIToggleButton.h"

@interface RSSViewController : UIViewController<UICompositeViewDelegate> {
    BOOL alreadyLoaded;
}

@property (nonatomic, retain) IBOutlet RSSTableViewController* tableController;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) IBOutlet UIButton* newsButton;
@property (nonatomic, retain) IBOutlet UIButton* servicesButton;
@property (nonatomic, retain) IBOutlet UIButton* agendaButton;
@property (nonatomic, retain) IBOutlet UIButton* aboutButton;
@property (retain, nonatomic) IBOutlet UIButton *condominioButton;
@property (retain, nonatomic) IBOutlet UIButton *opensButton;
@property (retain, nonatomic) IBOutlet UIButton *construtoraButton;
@property (retain, nonatomic) IBOutlet UIView *extraBar;
@property (nonatomic) NSInteger *barra;

@end

