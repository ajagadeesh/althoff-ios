/* WebViewController.m
 *
 * Copyright (C) 2009  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#import "WebViewController.h"

@implementation WebViewController

@synthesize licensesView;


#pragma mark - Lifecycle Functions


- (void)dealloc {
    [licensesView release];

    [super dealloc];
}



#pragma mark - ViewController Functions
- (void)viewDidLoad {
    
    [super viewDidLoad];
}

    
-(void)setupURLS:(NSString *)urlString
{
    
    NSString *urlAddress = [NSString stringWithFormat:@"%@", urlString];
    NSLog(@"URL ADDRESS %@", urlAddress);

	   
    if([LinphoneManager runningOnIpad]) {
        [LinphoneUtils adjustFontSize:self.view mult:2.22f];
    }
    
    
    NSURL *url = [NSURL URLWithString: urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    NSLog(@"URL ADDRESS WITH AUTH %@", url);

    // Load the resource using the request
    [licensesView setDelegate:self];
    [licensesView loadRequest:requestObj];
      
        
        [super viewDidLoad];
}


#pragma mark - UICompositeViewDelegate Functions

static UICompositeViewDescription *compositeDescription = nil;

+ (UICompositeViewDescription *)compositeViewDescription {
    if(compositeDescription == nil) {
        compositeDescription = [[UICompositeViewDescription alloc] init:@"WebView"
                                                                content:@"WebViewController"
                                                               stateBar:nil
                                                        stateBarEnabled:false
                                                                 tabBar:@"UIWebBar"
                                                          tabBarEnabled:true
                                                             fullscreen:false
                                                          landscapeMode:true
                                                           portraitMode:true];
    }
    return compositeDescription;
}

@end
