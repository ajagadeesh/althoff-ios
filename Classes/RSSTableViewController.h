/* HistoryTableViewController.h
 *
 * Copyright (C) 2009  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or   
 *  (at your option) any later version.                                 
 *                                                                      
 *  This program is distributed in the hope that it will be useful,     
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of      
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 *  GNU General Public License for more details.                
 *                                                                      
 *  You should have received a copy of the GNU General Public License   
 *  along with this program; if not, write to the Free Software         
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */     

#import <UIKit/UIKit.h>
#import "RSSKit.h"

@interface ModelRSS : NSObject
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* URLAddress;
@property (nonatomic, strong) NSString* URLImage;
-(id)initWithContent:(NSString*)theContent
       andURLAddress:(NSString*)theURLAddress
         andURLImage:(NSString*)theURLImage;
@end

@interface ModelRSSService : ModelRSS
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* number;
-(id)initWithContent:(NSString*)theContent
            AndTitle:(NSString*)theTitle
         andURLImage:(NSString*)theURLImage
       andURLAddress:(NSString*)theURLAddress
           andNumber:(NSString*)theNumber;
@end

@class  Reachability;
@interface RSSTableViewController : UITableViewController<RSSParserDelegate, NSURLConnectionDelegate> {
    @private
    NSMutableArray *rssEntries;
    NSMutableArray *rssEntriesLoaded;
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    BOOL alreadyWarned;
    BOOL alreadyLoaded;
}
- (void)loadRSS;

@property (nonatomic, retain) NSString *urlAddress;

@property (nonatomic, assign) BOOL missedFilter;
@property (nonatomic, assign) BOOL isService;
@property (retain, nonatomic) NSString *rssAddress;
@property (nonatomic, retain) NSString *resourceAddress;

@end
