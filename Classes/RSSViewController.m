/* HistoryViewController.m
 *
 * Copyright (C) 2012  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or   
 *  (at your option) any later version.                                 
 *                                                                      
 *  This program is distributed in the hope that it will be useful,     
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of      
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 *  GNU General Public License for more details.                
 *                                                                      
 *  You should have received a copy of the GNU General Public License   
 *  along with this program; if not, write to the Free Software         
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */              

#import "RSSViewController.h"
#import "WebViewController.h"
#import "PhoneMainView.h"
#import "NetworkUtils.h"

@implementation RSSViewController

@synthesize tableView;
@synthesize tableController;


@synthesize newsButton;
@synthesize servicesButton;
@synthesize agendaButton;
@synthesize aboutButton;
@synthesize condominioButton;
@synthesize opensButton;
@synthesize construtoraButton;

typedef enum _HistoryView {
    History_All,
    History_Missed,
    History_MAX
} HistoryView;

#pragma mark - Lifecycle Functions

- (id)init {
    alreadyLoaded = NO;
    return [super initWithNibName:@"RSSViewController" bundle:[NSBundle mainBundle]];
}

- (void)dealloc {
    [tableController release];
    [tableView release];
    
    [newsButton release];
    [servicesButton release];
    [agendaButton release];
	[aboutButton release];
    [condominioButton release];
    [opensButton release];
    [construtoraButton release];
    [_extraBar release];
    [super dealloc];
}


#pragma mark - UICompositeViewDelegate Functions

static UICompositeViewDescription *compositeDescription = nil;

+ (UICompositeViewDescription *)compositeViewDescription {
    if(compositeDescription == nil) {
        compositeDescription = [[UICompositeViewDescription alloc] init:@"RSS" 
                                                                content:@"RSSViewController" 
                                                               stateBar:nil 
                                                        stateBarEnabled:false 
                                                                 tabBar:@"UIMainBar" 
                                                          tabBarEnabled:true 
                                                             fullscreen:false
                                                          landscapeMode:[LinphoneManager runningOnIpad]
                                                         // landscapeMode:true
                                                           portraitMode:true];
    }
    return compositeDescription;
}

#pragma mark - ViewController Functions

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([[UIDevice currentDevice].systemVersion doubleValue] < 5.0) {
        [tableController viewWillAppear:animated];
    }
    
//    if([tableController isEditing]) {
        [tableController setEditing:FALSE animated:FALSE];
//    }
//    [aboutButton setHidden:TRUE];

//    [self changeView: History_All];
    
    // Reset missed call
//    linphone_core_reset_missed_calls_count([LinphoneManager getLc]);
    
    if (!alreadyLoaded)
        [self.extraBar setHidden:NO];
    
    // Fake event
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCallUpdate object:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[UIDevice currentDevice].systemVersion doubleValue] < 5.0) {
        [tableController viewDidAppear:animated];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if ([[UIDevice currentDevice].systemVersion doubleValue] < 5.0) {
        [tableController viewDidDisappear:animated];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([[UIDevice currentDevice].systemVersion doubleValue] < 5.0) {
        [tableController viewWillDisappear:animated];
    }
}

#define BARSIZE 44


- (void)viewDidLoad {
    

    
       NSInteger barra = 0;
    if ([LinphoneManager runningOnIpad]) {
        barra = 10;
    }
    
    [super viewDidLoad];
    [self changeView: History_All];
/*
    // Set selected+over background: IB lack !
    [editButton setBackgroundImage:[UIImage imageNamed:@"history_edit_default"]
                          forState:(UIControlStateSelected)];

//    [LinphoneUtils buttonFixStates:editButton];
    
    // Set selected+over background: IB lack !
    [allButton setBackgroundImage:[UIImage imageNamed:@"services_all_selected.png"] 
                    forState:(UIControlStateSelected)];
    
//    [LinphoneUtils buttonFixStatesForTabs:allButton];
    
    // Set selected+over background: IB lack !
    [missedButton setBackgroundImage:[UIImage imageNamed:@"services_all_selected.png"] 
               forState:(UIControlStateHighlighted | UIControlStateSelected)];

    [condominioButton setBackgroundImage:[UIImage imageNamed:@"condominio_all_selected.png"]
                            forState:(UIControlStateSelected)];
    
    [opensButton setBackgroundImage:[UIImage imageNamed:@"opens_all_selected.png"]
                            forState:(UIControlStateSelected)];
    
    [construtoraButton setBackgroundImage:[UIImage imageNamed:@"construtora_all_selected.png"]
                            forState:(UIControlStateSelected)];
    
//    [LinphoneUtils buttonFixStatesForTabs:missedButton];
*/
    if (!alreadyLoaded) {
        newsButton.selected = YES;
        servicesButton.selected = NO;
        condominioButton.selected = YES;
        
        [self.extraBar setHidden:NO];
//        self.tableView.frame = CGRectMake(0, BARSIZE, self.tableView.frame.size.width, self.tableView.frame.size.height);
        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y + (BARSIZE + barra), self.tableView.frame.size.width, self.tableView.frame.size.height);
        
        alreadyLoaded = YES;
    }
    
    [tableController.tableView setBackgroundColor:[UIColor clearColor]]; // Can't do it in Xib: issue with ios4
    [tableController.tableView setBackgroundView:nil]; // Can't do it in Xib: issue with ios4
}

- (IBAction)onAboutTouched:(id)sender {
    [[PhoneMainView instance] changeCurrentView:[AboutViewController compositeViewDescription] push:TRUE];
}


#pragma mark -

- (void)changeView: (HistoryView) view {
    /*
    if(view == History_All) {
        allButton.selected = TRUE;
        [tableController setMissedFilter:FALSE];
    } else {
        allButton.selected = FALSE;
    }
    
    if(view == History_Missed) {
        missedButton.selected = TRUE;
        [tableController setMissedFilter:TRUE];
    } else {
        missedButton.selected = FALSE;
    }*/
}


#pragma mark - Action Functions


- (IBAction)onReservation:(id)sender {
    
/*
    if (!self.extraBar.hidden) {
        [self.extraBar setHidden:YES];
        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y - BARSIZE, self.tableView.frame.size.width, self.tableView.frame.size.height);
    }
    
    [self.tableView setHidden:YES];
    [self.detailWebView setHidden:NO];
*/

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *url = [[LinphoneManager instance] lpConfigStringForKey:@"api" forSection:@"app"];
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *sk = [[LinphoneManager instance] lpConfigStringForKey:@"secret_key" forSection:@"app"];
    sk = [sk stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"password"]) {
        NSString *authKey = [NetworkUtils getAuthKeyWithSalt:[defaults objectForKey:@"password"] andMoreSalt:sk];
        NSString *str = [NSString stringWithFormat:url, [defaults objectForKey:@"username"], [NetworkUtils getMD5:authKey]];
 
       NSString *theURL = [NSString stringWithFormat:@"%@%@", rootURL, str];
        
        NSLog(@"LOGIN WITH AUTH %@ AUTH KEY %@", theURL, authKey);
        

        WebViewController *controller = DYNAMIC_CAST([[PhoneMainView instance] changeCurrentView:[WebViewController compositeViewDescription]  push:TRUE], WebViewController);
      
//        WebViewController *controller = DYNAMIC_CAST([[PhoneMainView instance] changeCurrentView:[WebViewController compositeViewDescription]  push:TRUE], WebViewController);
        
        [controller setupURLS: theURL];
    }
 }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
}

- (IBAction)onNews:(id)sender {
    NSInteger barra = 0;
    if ([LinphoneManager runningOnIpad]) {
        barra = 10;
    }
    
    if (self.extraBar.hidden) {
        [self.extraBar setHidden:NO];
        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y + (BARSIZE + barra), self.tableView.frame.size.width, self.tableView.frame.size.height);
    }
    [self.tableView setHidden:NO];
    newsButton.selected = YES;
    servicesButton.selected = NO;
    opensButton.selected = NO;
    construtoraButton.selected = NO;
    condominioButton.selected = YES;
    
    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"condominio" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = NO;
    self.tableController.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.tableController.rssAddress];
    [self.tableController loadRSS];
    
    
    
    /*
    opensButton.selected = NO;
    opensButton.selected = NO;
    construtoraButton.selected = NO;
    
    
    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"news" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = NO;
    [self.tableController loadRSS];
     */
}

- (IBAction)onServices:(id)sender {
    NSInteger barra = 0;
    if ([LinphoneManager runningOnIpad]) {
        barra = 10;
    }
    
    if (!self.extraBar.hidden) {
        [self.extraBar setHidden:YES];
        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y - (BARSIZE + barra), self.tableView.frame.size.width, self.tableView.frame.size.height);
    }

    [self.tableView setHidden:NO];
    newsButton.selected = NO;
    opensButton.selected = NO;
    opensButton.selected = NO;
    construtoraButton.selected = NO;
    servicesButton.selected = YES;

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];


    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"services" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = YES;
    self.tableController.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.tableController.rssAddress];
    [self.tableController loadRSS];
}

- (IBAction)onCondominio:(id)sender {
    [self.tableView setHidden:NO];
    newsButton.selected = YES;
    servicesButton.selected = NO;
    opensButton.selected = NO;
    construtoraButton.selected = NO;
    condominioButton.selected = YES;

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];


    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"condominio" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = NO;
    self.tableController.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.tableController.rssAddress];
    [self.tableController loadRSS];
}

- (IBAction)onOpens:(id)sender {
    [self.tableView setHidden:NO];
    condominioButton.selected = NO;
    newsButton.selected = YES;
    servicesButton.selected = NO;
    construtoraButton.selected = NO;
    opensButton.selected = YES;

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"opens" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = NO;
    self.tableController.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.tableController.rssAddress];
    [self.tableController loadRSS];
}

- (IBAction)onConstrutora:(id)sender {
    [self.tableView setHidden:NO];
    condominioButton.selected = NO;
    newsButton.selected = YES;
    servicesButton.selected = NO;
    opensButton.selected = NO;
    construtoraButton.selected = YES;

    NSString *rootURL = [[LinphoneManager instance] lpConfigStringForKey:@"root_url" forSection:@"rss"];
    rootURL = [rootURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];


    NSString *rss = [[LinphoneManager instance] lpConfigStringForKey:@"construtora" forSection:@"rss"];
    NSLog(@"Get rss from %@", rss);
    self.tableController.rssAddress = [rss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.tableController.isService = NO;
    self.tableController.rssAddress = [NSString stringWithFormat:@"%@%@", rootURL, self.tableController.rssAddress];
    [self.tableController loadRSS];
}

- (void)viewDidUnload {
    [self setExtraBar:nil];
    [super viewDidUnload];
}
@end
