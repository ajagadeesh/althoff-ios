//
//  ServiceDetailsViewController.m
//  softphone
//
//  Created by Eduardo Nunes Pereira on 16/7/13.
//
//

#import "ServiceDetailsViewController.h"
#import "PhoneMainView.h"

@interface ServiceDetailsViewController ()

@end

@implementation ServiceDetailsViewController

static UICompositeViewDescription *compositeDescription = nil;

+ (UICompositeViewDescription *)compositeViewDescription {
    if(compositeDescription == nil) {
        compositeDescription = [[UICompositeViewDescription alloc] init:@"ServiceDetails"
                                                                content:@"ServiceDetailsViewController"
                                                               stateBar:nil
                                                        stateBarEnabled:false
                                                                 tabBar:@"UIMainBar"
                                                          tabBarEnabled:true
                                                             fullscreen:false
                                                          landscapeMode:[LinphoneManager runningOnIpad]
                                                           portraitMode:true];
    }
    return compositeDescription;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCallButton:(id)sender {    
    [[LinphoneManager instance] call:self.contactNumber.text displayName:self.contactName.text transfer:FALSE];
}

- (IBAction)onCallByiPhoneButton:(id)sender {
    NSString *number = [NSString stringWithFormat:@"tel:%@", self.contactNumber.text];
    NSURL *phoneNumberURL = [NSURL URLWithString:number];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}

- (IBAction)onBackButton:(id)sender {
    [[PhoneMainView instance] popCurrentView];
}

-(void)setupWithImageURL:(NSString *)theURL
                 contact:(NSString *)theContact
             contactLink:(NSString *)theContactLink
           contactNumber:(NSString *)theContactNumber
      contactDescription:(NSString *)theContactDescription
{
    NSURL *imageURL = [NSURL URLWithString:theURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image.image = [UIImage imageWithData:imageData];
        });
    });
    
    self.contactName.text = theContact;
    self.contactLink.text = theContactLink;
    self.contactNumber.text = theContactNumber;
    self.contactDescription.text = theContactDescription;
}


- (IBAction)onLinkButton:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.contactLink.text]];
}

- (void)dealloc {
    [_image release];
    [_contactName release];
    [_contactLink release];
    [_contactNumber release];
    [_contactDescription release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setImage:nil];
    [self setContactName:nil];
    [self setContactLink:nil];
    [self setContactNumber:nil];
    [self setContactDescription:nil];
    [super viewDidUnload];
}
@end
