/* UILinphoneTextField.m
 *
 * Copyright (C) 2012  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#import "UILinphoneTextField.h"

@implementation UILinphoneTextField

#pragma mark - Lifecycle Functions

- (void)initUILinphoneTextField {
    self.layer.cornerRadius=0.0f;
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]CGColor];
    self.layer.borderWidth= 1.0f;
    [self setBorderStyle:UITextBorderStyleLine];
}

- (id)init {
    self = [super init];
    if(self != nil) {
        [self initUILinphoneTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self != nil) {
        [self initUILinphoneTextField];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self != nil) {
        [self initUILinphoneTextField];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}


- (void)drawRect:(CGRect)rect {
    // we ignore the rect and redraw the entire view
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (context) {
    }
}

@end
