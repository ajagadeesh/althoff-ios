//
//  RSSTableViewCell.h
//  softphone
//
//  Created by Eduardo Nunes Pereira on 11/7/13.
//
//

#import <UIKit/UIKit.h>
#import "PhoneMainView.h"

@interface RSSTableViewCell : UITableViewCell

@property (nonatomic, retain) NSString *urlAddress;
@property (retain, nonatomic) IBOutlet UITextView *title;
- (id)initWithIdentifier:(NSString*)identifier;
- (void)setupTitle: (NSString*) title;
- (void)setupURLS: (NSString*) urlString;
- (void)setupImageURL:(NSString*) urlImage;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@end
