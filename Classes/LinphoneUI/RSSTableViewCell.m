//
//  RSSTableViewCell.m
//  softphone
//
//  Created by Eduardo Nunes Pereira on 11/7/13.
//
//

#import "RSSTableViewCell.h"

@implementation RSSTableViewCell
@synthesize title = _title;
@synthesize imageView;


- (id)initWithIdentifier:(NSString*)identifier {
    if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier]) != nil) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"RSSTableViewCell"
                                                              owner:self
                                                            options:nil];
        
        if ([arrayOfViews count] >= 1) {
            [self addSubview:[[arrayOfViews objectAtIndex:0] retain]];
        }

        self.title.text = identifier;
        if([LinphoneManager runningOnIpad]) {
            UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
            [self.title setFont:font];
        }

    }
    return self;
}

-(void)setupTitle:(NSString *)title {
    self.title.text = title;
}

-(void)setupURLS:(NSString *)urlString
{
    self.urlAddress = urlString;
}

- (void)setupImageURL:(NSString *)urlImage
{
    if (!urlImage) return;
    
    NSURL *imageURL = [NSURL URLWithString:urlImage];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];

        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Load image '%@'", urlImage);
            self.imageView.image = [UIImage imageWithData:imageData];
        });
    });
}

/*
- (IBAction)onCellButton:(id)sender {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                  @"([0-9]+):([a-zA-Z]+)" options:0 error:nil];

    NSArray *matches = [regex matchesInString:self.urlAddress
                                      options:0
                                        range:NSMakeRange(0, [self.urlAddress length])];

    NSTextCheckingResult *match = matches[0];
    NSRange firstHalfRange = [match rangeAtIndex:1];
    NSRange secondHalfRange = [match rangeAtIndex:2];


    NSString *newURL = [regex stringByReplacingMatchesInString:self.urlAddress options:0 range:NSMakeRange(0, self.urlAddress.length) withTemplate:[NSString stringWithFormat:@"%@&from=%@", [self.urlAddress substringWithRange:firstHalfRange], [self.urlAddress substringWithRange:secondHalfRange]]];

    
    WebViewController *controller = DYNAMIC_CAST([[PhoneMainView instance] changeCurrentView:[WebViewController compositeViewDescription]  push:TRUE], WebViewController);
    
    [controller setupURLS: newURL];
 //   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newURL]];
}
*/

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_title release];

    [super dealloc];
}

- (void)setHighlighted:(BOOL)highlighted {
    [self setHighlighted:highlighted animated:FALSE];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if(highlighted) {
        [_title setTextColor:[UIColor whiteColor]];
    } else {
        [_title setTextColor:[UIColor  blackColor]];
    }
}
@end
