//
//  RSSTableViewCellImgCell.h
//  softphone
//
//  Created by Eduardo Nunes Pereira on 16/7/13.
//
//

#import <UIKit/UIKit.h>
#import "PhoneMainView.h"

@interface RSSTableViewCellImgCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UITextView *title;
@property (retain, nonatomic) IBOutlet UIImageView *imgTitle;
- (id)initWithIdentifier:(NSString*)identifier;
- (void)setupTitle: (NSString*) title;
- (void)setupImgURL: (NSString*) imgURL;
@end
