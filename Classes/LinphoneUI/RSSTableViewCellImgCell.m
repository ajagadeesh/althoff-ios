//
//  RSSTableViewCellImgCell.m
//  softphone
//
//  Created by Eduardo Nunes Pereira on 16/7/13.
//
//

#import "RSSTableViewCellImgCell.h"

@implementation RSSTableViewCellImgCell

@synthesize title = _title;
@synthesize imgTitle = imgTitle;

- (id)initWithIdentifier:(NSString*)identifier {
    if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier]) != nil) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"RSSTableViewCellImg"
                                                              owner:self
                                                            options:nil];
        
        if ([arrayOfViews count] >= 1) {
            [self addSubview:[[arrayOfViews objectAtIndex:0] retain]];
        }
        
        self.title.text = identifier;
        if([LinphoneManager runningOnIpad]) {
            UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
            [self.title setFont:font];
        }
        
    }
    return self;
}

-(void)setupTitle:(NSString *)title
{
    self.title.text = title;
}

-(void)setupImgURL:(NSString *)imgURL
{
    if (!imgURL) return;

    NSURL *imageURL = [NSURL URLWithString:imgURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgTitle.image = [UIImage imageWithData:imageData];
        });
    });
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
    [_title release];
    [imgTitle release];
    [super dealloc];
}

- (void)setHighlighted:(BOOL)highlighted {
    [self setHighlighted:highlighted animated:FALSE];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if(highlighted) {
        [_title setTextColor:[UIColor whiteColor]];
    } else {
        [_title setTextColor:[UIColor  blackColor]];
    }
}

@end
