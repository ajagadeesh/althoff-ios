//
//  EvAPPNetworkUtils.m
//  EventAPP
//
//  Created by Eduardo Nunes Pereira on 25/7/13.
//  Copyright (c) 2013 Mobin Solutions. All rights reserved.
//

#import "NetworkUtils.h"
#import "NICInfo.h"
#import "NICInfoSummary.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NetworkUtils

+ (NSString *)getAuthKeyWithSalt:(NSString *)salt andMoreSalt:(NSString *)moreSalt {
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"ddMMyyyy"];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSString *authKey = [NSString stringWithFormat:@"%@%@%@", dateString, salt, moreSalt];
    return authKey;
}

+ (NSString *)getMD5:(NSString *)input {
    NSLog(@"INPUT MD5 %@", input);
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

    NSLog(@"DIGEST LEN %d", CC_MD5_DIGEST_LENGTH);

    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];

    return  output;
}

@end
