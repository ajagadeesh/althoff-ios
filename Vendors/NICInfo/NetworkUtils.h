//
//  EvAPPNetworkUtils.h
//  EventAPP
//
//  Created by Eduardo Nunes Pereira on 25/7/13.
//  Copyright (c) 2013 Mobin Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkUtils : NSObject
+ (NSString*)getAuthKeyWithSalt:(NSString *)salt andMoreSalt:(NSString *) moreSalt;
+ (NSString *)getMD5:(NSString *)input;
@end
